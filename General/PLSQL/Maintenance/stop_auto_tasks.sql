conn / as sysdba

SET SERVEROUTPUT ON
BEGIN
        DBMS_AUTO_TASK_ADMIN.DISABLE('sql tuning advisor',null,null);
        DBMS_AUTO_TASK_ADMIN.DISABLE('auto space advisor',null,null);
END;
/

select client_name, status from dba_autotask_client;

exit


