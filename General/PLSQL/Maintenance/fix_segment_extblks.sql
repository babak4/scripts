connect / as sysdba

begin
  for t in (
    select distinct tablespace_name
    from sys.sys_dba_segs
    where bitand(segment_flags,131073) = 1
      and segment_type not in ('ROLLBACK', 'DEFERRED ROLLBACK', 'TYPE2 UNDO')
      and tablespace_name != 'SYSTEM'
  )
  loop
    dbms_space_admin.tablespace_fix_segment_extblks(t.tablespace_name);
  end loop;
end;
/

exit;

