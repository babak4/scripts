conn / as sysdba
set linesize 200
column name format a30
column size format a10
select name, case when value = 0 then 'Not Set' when value/1024/1024/1024 < 1 then to_char(value/1024/1024) || 'MB' else to_char(value/1024/1024/1024) || 'GB' end "size"
from v$parameter
where  name in('sga_max_size','sga_target', 'pga_aggregate_target','db_cache_size','java_pool_size','streams_pool_size','large_pool_size','memory_max_target', 'memory_target')
order by 1;
exit

