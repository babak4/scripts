conn / as sysdba

--alter system set db_cache_size=6G scope=spfile;
--alter system set java_pool_size=500M scope=spfile;
--alter system set large_pool_size=100m scope=spfile;
alter system reset memory_max_target scope=spfile;
alter system reset memory_target scope=spfile;
alter system set pga_aggregate_target=4G scope=spfile;
alter system set sga_max_size=8G scope=spfile;
alter system set sga_target=6G scope=spfile;
--alter system set shared_pool_size=6G scope=spfile;
--alter system set streams_pool_size=100M scope=spfile;
startup force;
exit;
