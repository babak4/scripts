conn / as sysdba

SET SERVEROUTPUT ON
DECLARE

	v_runpoint				VARCHAR2(300) := 'start';
	v_SourceTablespace      DBA_EXTENTS.TABLESPACE_NAME%TYPE := 'ACTIX_STAGE_0001';
	v_DestinationTablespace DBA_EXTENTS.TABLESPACE_NAME%TYPE := 'ACTIX_SHRINK_TABLESPACE';
	v_SourceFileID          PLS_INTEGER ;
	v_Threshold_BLOCK_ID	PLS_INTEGER := 0;	
	v_ParallelThreshold		NUMBER;
	v_TS#					PLS_INTEGER;
	v_DBA_Extent_Method		PLS_INTEGER := 1; -- 1: using externalized sys objects, 2: using DBA_EXTENTS itself
	v_moved 			NUMBER := -1;
	v_SQL 				VARCHAR2(32767);
	v_msg 				VARCHAR2(100);
	v_statement			VARCHAR2(300);
	C_Cursor	  		SYS_REFCURSOR;
	v_TotalSegmentCount NUMBER;
	v_SegmentCount		NUMBER := 0;
	v_WorkingDirectory		VARCHAR2(100) := '/home/oracle/babak/tablespace_maintenance';
	v_LogFileName			VARCHAR2(100) := CASE WHEN v_DestinationTablespace = 'ACTIX_SHRINK_TABLESPACE' THEN 'ActixShrinkLog_' ELSE 'ActixMoveLog_' END|| v_SourceTablespace || '_'|| TO_CHAR(SYSDATE, 'DDMONYYYYHH24MI') || '.log';
	v_LogFileHandle			UTL_FILE.FILE_TYPE;

	Type SegmentRow IS RECORD
	(
		TABLESPACE_NAME	DBA_EXTENTS.TABLESPACE_NAME%TYPE,
		OWNER			DBA_EXTENTS.OWNER%TYPE, 
		SEGMENT_TYPE	DBA_EXTENTS.SEGMENT_TYPE%TYPE, 
		SEGMENT_NAME	DBA_EXTENTS.SEGMENT_NAME%TYPE, 
		PARTITION_NAME	DBA_EXTENTS.PARTITION_NAME%TYPE, 
		BLOCKID			DBA_EXTENTS.BLOCK_ID%TYPE,
		BLOCKS			DBA_EXTENTS.BLOCKS%TYPE,
		IOT				NUMBER(1),
		IOT_SEGMENT_NAME	DBA_EXTENTS.SEGMENT_NAME%TYPE
	);
	v_Segment			SegmentRow;
	
	TYPE ActixExtent IS RECORD
	(
		OWNER               VARCHAR2(30),
		TABLESPACE_NAME     VARCHAR2(30),
		SEGMENT_TYPE        VARCHAR2(18),
		SEGMENT_NAME        VARCHAR2(81),
		PARTITION_NAME      VARCHAR2(30),
		BLOCK_ID            NUMBER,
		BLOCKS				NUMBER,
		IOT					NUMBER(1),
		IOT_SEGMENT_NAME	VARCHAR2(81)
	);
	
	v_actixExtent ActixExtent;
	
	PROCEDURE LOG
		(
			p_LogFileHandle		UTL_FILE.FILE_TYPE,
			p_MSG				VARCHAR2,
			p_NewLinePrefix		NUMBER DEFAULT 1
		)
	IS
	BEGIN
		IF p_NewLinePrefix = 1 THEN
			UTL_FILE.NEW_LINE(p_LogFileHandle,1);
		END IF;
		UTL_FILE.PUT_LINE(p_LogFileHandle,p_MSG);
		UTL_FILE.FFLUSH(p_LogFileHandle);
	END LOG;
	
	PROCEDURE OPEN_LOG_FILE
	IS
	BEGIN
		v_SQL := 'CREATE OR REPLACE DIRECTORY ACTIX_SHRINK_EXTENTS_DIR AS ''' || v_WorkingDirectory || '''';
		EXECUTE IMMEDIATE v_SQL;
		--EXECUTE IMMEDIATE 'GRANT READ,WRITE ON ACTIX_SHRINK_EXTENTS_DIR TO PUBLIC';
		v_LogFileHandle := UTL_FILE.FOPEN('ACTIX_SHRINK_EXTENTS_DIR',v_LogFileName,'w');

		DBMS_OUTPUT.PUT_LINE('Log File Opened: ' || v_WorkingDirectory || '/' || v_LogFileName);
		
		LOG(v_LogFileHandle,'Date-Time: ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
		LOG(v_LogFileHandle,'Source Tablespace: ' || v_SourceTablespace);
		LOG(v_LogFileHandle,'Source FileID: ' || TO_CHAR(v_SourceFileID));
		LOG(v_LogFileHandle,'Destination Tablespace: ' || v_DestinationTablespace);
		LOG(v_LogFileHandle,' ');

	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE(SQLCODE || ':' || SQLERRM);
			DBMS_OUTPUT.PUT_LINE('It was not possible to Create directory ACTIX_SHRINK_EXTENTS_DIR.');
			RETURN;
	END OPEN_LOG_FILE;
	
	PROCEDURE CLOSE_LOG_FILE
	IS
	BEGIN
		LOG(v_LogFileHandle,'Finished MoveSegments at ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
		UTL_FILE.FCLOSE(v_LogFileHandle);  
		EXECUTE IMMEDIATE 'DROP DIRECTORY ACTIX_SHRINK_EXTENTS_DIR';
	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE(SQLCODE || ':' || SQLERRM);
			RETURN;
	END CLOSE_LOG_FILE;
	
	PROCEDURE RUN_STATEMENT
	(
		p_LogFileHandle	UTL_FILE.FILE_TYPE,
		p_runpoint		VARCHAR2,
		p_statement		VARCHAR2
	)
	IS
	BEGIN
		EXECUTE IMMEDIATE p_statement;
	EXCEPTION
		WHEN OTHERS THEN
				LOG(p_LogFileHandle,p_runpoint);
				LOG(p_LogFileHandle,p_statement);
				LOG(p_LogFileHandle,SQLERRM);
	END RUN_STATEMENT;

	PROCEDURE CALCULATE_PARALLEL_BLK_LIMIT
	IS
	BEGIN
		v_runpoint := 'Calculating BLOCKS threshold for Parallelism';
		EXECUTE IMMEDIATE 'SELECT ROUND(AVG(BLOCKS)) FROM ACTIX_SHRINK_EXTENTS'
		INTO v_ParallelThreshold;
	END CALCULATE_PARALLEL_BLK_LIMIT;

	PROCEDURE Create_Segment_list
	IS
		v_TableName			VARCHAR2(30);
		v_PartitionName		VARCHAR2(30);
		v_LOB_Column_Name	VARCHAR2(30);
		v_CNT				NUMBER;
		
		PROCEDURE DROP_TABLE
		(
			p_TABLE_NAME	VARCHAR2
		)
		IS
			v_TABLE_Exists	PLS_INTEGER;	
		BEGIN
			v_runpoint := 'Create_Segment_list.DROP_TABLE.START - p_TABLE_NAME:= '||p_TABLE_NAME;
			SELECT /*+ FIRST_ROWS(1) */ COUNT(*)
			INTO v_TABLE_Exists
			FROM ALL_OBJECTS
			WHERE OWNER = USER
			AND OBJECT_TYPE = 'TABLE'
			AND OBJECT_NAME = p_TABLE_NAME
			AND ROWNUM = 1;

			v_runpoint := 'Create_Segment_list.DROP_TABLE.PRE_DROP - v_TABLE_Exists:' || TO_CHAR(v_TABLE_Exists);
			IF v_TABLE_Exists <> 0 THEN
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, 'DROP TABLE ' || p_TABLE_NAME || ' PURGE');
			END IF;
			v_runpoint := 'Create_Segment_list.DROP_TABLE.POST_DROP';
		END DROP_TABLE;
			
		PROCEDURE REPLACE_SYS_SEGMENTS
		IS
		BEGIN
			LOG(v_LogFileHandle,'Starting SYS Segment replacement...');
			v_SQL := 'SELECT *
						FROM ACTIX_SHRINK_EXTENTS
						WHERE SEGMENT_NAME LIKE ''SYS%''
						ORDER BY BLOCK_ID DESC';
			OPEN C_Cursor FOR v_SQL;
			LOOP
				FETCH C_Cursor INTO v_actixExtent;
				EXIT WHEN C_Cursor%NOTFOUND;

				LOG(v_LogFileHandle,'Original Segment (' || v_actixExtent.SEGMENT_TYPE || '): ' || v_actixExtent.OWNER || '.' || v_actixExtent.SEGMENT_NAME || '(' || v_actixExtent.PARTITION_NAME || ')');
	---------------------
	-- INDEX PARTITION --
	---------------------
				IF v_actixExtent.SEGMENT_TYPE='INDEX PARTITION' THEN
				
					v_runpoint := 'SYS% segments - INDEX PARTITION - 1';
					v_SQL := 'SELECT /*+ MONITOR */ TABLE_NAME,
									(SELECT /*+ NO_UNNEST */ MAX(PARTITION_NAME) 
									FROM ACTIX_LOB_PARTITIONS 
									where TABLE_NAME = al.TABLE_NAME 
									AND LOB_NAME = al.SEGMENT_NAME 
									AND TABLE_OWNER = al.OWNER
									AND LOB_INDPART_NAME = :v_PARTITION_NAME)
								FROM ACTIX_LOBS al
								WHERE INDEX_NAME= :v_SEGMENT_NAME
								AND OWNER = :v_OWNER';
							
					EXECUTE IMMEDIATE v_SQL
					INTO v_TableName, v_PartitionName
					USING v_actixExtent.PARTITION_NAME, v_actixExtent.SEGMENT_NAME, v_actixExtent.OWNER;
					
					
					LOG(v_LogFileHandle,'Equivalent Segment: ' || v_actixExtent.OWNER || '.' || v_TableName || '(' || v_PartitionName || ')');
					LOG(v_LogFileHandle,'Updating Original Segment''s Record with the Equivalent segment');
					-- Updates the Original segment's record, if the equivalent record doesn't already exist in ACTIX_SHRINK_EXTENTS
					v_runpoint := 'SYS% segments - INDEX PARTITION - 2';
					v_SQL := 'UPDATE /*+ MONITOR */ ACTIX_SHRINK_EXTENTS
							SET SEGMENT_NAME = :v_TableName,
							PARTITION_NAME = :v_PartitionName,
							SEGMENT_TYPE = ''TABLE PARTITION''
							WHERE OWNER = :v_OWNER
							AND TABLESPACE_NAME = :v_TABLESPACE_NAME
							AND SEGMENT_NAME = :v_SEGMENT_NAME
							AND NOT EXISTS
										(
											SELECT NULL
											FROM ACTIX_SHRINK_EXTENTS
											WHERE SEGMENT_NAME = :v_TableName
											AND PARTITION_NAME = :v_PartitionName
											AND SEGMENT_TYPE = ''TABLE PARTITION''
											AND OWNER = :v_OWNER
											AND TABLESPACE_NAME = :v_TABLESPACE_NAME
										)';

					EXECUTE IMMEDIATE v_SQL
					USING v_TableName, v_PartitionName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME, v_actixExtent.SEGMENT_NAME,
						v_TableName, v_PartitionName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME;

					IF SQL%ROWCOUNT = 0 THEN
	--
	-- INDEX PARTITION's equivalent already exist in ACTIX_SHRINK_EXTENTS
	--
						LOG(v_LogFileHandle,'Equivalent segment already exists in ACTIX_SHRINK_EXTENTS; Dropping the Original segment record.');
						-- Deletes the Original segment, if the equivalent is previously found
						v_runpoint := 'SYS% segments - INDEX PARTITION - 3';
						v_SQL := 'DELETE FROM ACTIX_SHRINK_EXTENTS
									WHERE OWNER = :v_OWNER
									AND SEGMENT_NAME = :v_SEGMENT_NAME
									AND PARTITION_NAME = :v_PARTITION_NAME
									AND EXISTS (
												SELECT NULL
												FROM ACTIX_SHRINK_EXTENTS
												WHERE OWNER = :v_OWNER
												AND SEGMENT_NAME = :v_TableName
												AND PARTITION_NAME = :v_PartitionName								
												)';
									
						EXECUTE IMMEDIATE v_SQL
						USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME, v_actixExtent.PARTITION_NAME,
								v_actixExtent.OWNER, v_TableName, v_PartitionName;

						IF SQL%ROWCOUNT <> 0 THEN
							LOG(v_LogFileHandle,'Original Segment''s Record is deleted from ACTIX_SHRINK_EXTENTS');
						END IF;
					END IF;
		  
	-------------------
	-- LOB PARTITION --
	-------------------
				ELSIF v_actixExtent.SEGMENT_TYPE='LOB PARTITION' THEN

					v_runpoint := 'SYS% segments - LOB PARTITION - 1';
					v_SQL := 'SELECT /*+ MONITOR */ MAX(TABLE_NAME), MAX(PARTITION_NAME)
								FROM ACTIX_LOB_PARTITIONS
								WHERE LOB_NAME = :v_SEGMENT_NAME
								AND LOB_PARTITION_NAME = :v_PARTITION_NAME
								AND TABLE_OWNER = :v_OWNER';
								
					EXECUTE IMMEDIATE v_SQL
					INTO v_TableName, v_PartitionName
					USING v_actixExtent.SEGMENT_NAME, v_actixExtent.PARTITION_NAME, v_actixExtent.OWNER;

					LOG(v_LogFileHandle,'Equivalent Segment: ' || v_actixExtent.OWNER || '.' || v_TableName || '(' || v_PartitionName || ')');
					LOG(v_LogFileHandle,'Updating Original Segment''s Record with the Equivalent segment');
					-- Updates the Original segment's record, if the equivalent record doesn't already exist in ACTIX_SHRINK_EXTENTS
					v_runpoint := 'SYS% segments - LOB PARTITION - 2';
					v_SQL := 'UPDATE /*+ MONITOR */ ACTIX_SHRINK_EXTENTS
							SET SEGMENT_NAME = :v_TableName,
							PARTITION_NAME = :v_PartitionName,
							SEGMENT_TYPE = ''TABLE PARTITION''
							WHERE OWNER = :v_OWNER
							AND TABLESPACE_NAME = :v_TABLESPACE_NAME
							AND SEGMENT_NAME = :v_SEGMENT_NAME
							AND NOT EXISTS
										(
											SELECT NULL
											FROM ACTIX_SHRINK_EXTENTS
											WHERE SEGMENT_NAME = :v_TableName
											AND PARTITION_NAME = :v_PartitionName
											AND SEGMENT_TYPE = ''TABLE PARTITION''
											AND OWNER = :v_OWNER
											AND TABLESPACE_NAME = :v_TABLESPACE_NAME
										)';

					EXECUTE IMMEDIATE v_SQL
					USING v_TableName, v_PartitionName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME, v_actixExtent.SEGMENT_NAME,
						v_TableName, v_PartitionName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME;
					IF SQL%ROWCOUNT = 0 THEN
	--
	-- LOB PARTITION's equivalent not in ACTIX_SHRINK_EXTENTS
	--
						LOG(v_LogFileHandle,'Equivalent segment already exists in ACTIX_SHRINK_EXTENTS; Dropping the Original segment record.');
						-- Deletes the Original segment, if the equivalent is previously found
						v_runpoint := 'SYS% segments - LOB PARTITION - 3';
						v_SQL := 'DELETE FROM ACTIX_SHRINK_EXTENTS
									WHERE OWNER = :v_OWNER
									AND SEGMENT_NAME = :v_SEGMENT_NAME
									AND PARTITION_NAME = :v_PARTITION_NAME
									AND EXISTS (
												SELECT NULL
												FROM ACTIX_SHRINK_EXTENTS
												WHERE OWNER = :v_OWNER
												AND SEGMENT_NAME = :v_TableName
												AND PARTITION_NAME = :v_PartitionName								
												)';
									
						EXECUTE IMMEDIATE v_SQL
						USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME, v_actixExtent.PARTITION_NAME,
								v_actixExtent.OWNER, v_TableName, v_PartitionName;
			
						IF SQL%ROWCOUNT <> 0 THEN
							LOG(v_LogFileHandle,'Original Segment''s Record is deleted from ACTIX_SHRINK_EXTENTS');
						END IF;

					END IF;

	--------------
	-- LOBINDEX --
	--------------
				ELSIF v_actixExtent.SEGMENT_TYPE='LOBINDEX' AND v_actixExtent.SEGMENT_NAME LIKE 'SYS_IL%' THEN
		  
					v_runpoint := 'SYS% segments - LOBINDEX - 1';
					BEGIN
						v_SQL := 'SELECT /*+ FIRST_ROWS(1) MONITOR */ TABLE_NAME
								  FROM ACTIX_LOBS
								  WHERE OWNER = :v_OWNER
								  AND INDEX_NAME = :v_SEGMENT_NAME
								  AND TABLESPACE_NAME = :v_TABLESPACE_NAME
								  AND ROWNUM = 1';

						EXECUTE IMMEDIATE v_SQL
						INTO v_TableName
						USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME, v_actixExtent.TABLESPACE_NAME;
						
					EXCEPTION
						WHEN OTHERS
							THEN
								v_SQL := 'SELECT /*+ FIRST_ROWS(1) MONITOR */ MAX(TABLE_NAME)
										  FROM DBA_LOBS
										  WHERE OWNER = :v_OWNER
										  AND INDEX_NAME = :v_SEGMENT_NAME
										  AND TABLESPACE_NAME = :v_TABLESPACE_NAME
										  AND ROWNUM = 1';
										  
								EXECUTE IMMEDIATE v_SQL
								INTO v_TableName
								USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME, v_actixExtent.TABLESPACE_NAME;
					END;
					
					IF v_TableName IS NOT NULL THEN
		  --dbms_output.PUT_LINE('Equivalent Segment found.');

						SELECT REPLACE(MAX(COLUMN_NAME),'"','')
						INTO v_LOB_Column_Name
						FROM DBA_LOBS
						WHERE OWNER = v_actixExtent.OWNER
						AND INDEX_NAME= v_actixExtent.SEGMENT_NAME
						AND TABLE_NAME = v_TableName
						AND NOT EXISTS
									(
										SELECT NULL 
										FROM ALL_SDO_INDEX_METADATA
										WHERE SDO_INDEX_TABLE = v_TableName
									);

						LOG(v_LogFileHandle,'Equivalent Segment: ' || v_actixExtent.OWNER || '.' || v_TableName);
						
						v_runpoint := 'SYS% segments - LOBINDEX - 2';
						v_SQL := 'UPDATE /*+ MONITOR */ ACTIX_SHRINK_EXTENTS
									SET SEGMENT_NAME = :v_TableName,' || CASE WHEN v_LOB_Column_Name IS NOT NULL THEN '
									IOT_SEGMENT_NAME = ''' || v_LOB_Column_Name || ''',' END || '
									SEGMENT_TYPE = ''TABLE''
									WHERE OWNER = :v_OWNER
									AND TABLESPACE_NAME = :v_TABLESPACE_NAME
									AND SEGMENT_NAME = :v_SEGMENT_NAME
									AND NOT EXISTS
												(
													SELECT NULL
													FROM ACTIX_SHRINK_EXTENTS
													WHERE SEGMENT_NAME = :v_TableName
													AND SEGMENT_TYPE = ''TABLE''
													AND OWNER = :v_OWNER
													AND TABLESPACE_NAME = :v_TABLESPACE_NAME
												)';

						EXECUTE IMMEDIATE v_SQL
						USING v_TableName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME, v_actixExtent.SEGMENT_NAME,
								v_TableName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME;
								
						IF SQL%ROWCOUNT = 0 THEN
							LOG(v_LogFileHandle,'Equivalent Segment already exists in ACTIX_SHRINK_EXTENTS');

							v_SQL := 'DELETE FROM ACTIX_SHRINK_EXTENTS
										WHERE OWNER = :v_OWNER
										AND SEGMENT_NAME = :v_SEGMENT_NAME';
												
							EXECUTE IMMEDIATE v_SQL
							USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME;

							IF SQL%ROWCOUNT <> 0 THEN
								LOG(v_LogFileHandle,'Original Segment dropped from ACTIX_SHRINK_EXTENTS');
							END IF;
						END IF;
	--
	-- LOBINDEX's equivalent not in ACTIX_SHRINK_EXTENTS
	--
					ELSE 
						LOG(v_LogFileHandle,'Equivalent Segment not found');
					END IF;

	----------------
	-- LOBSEGMENT --
	----------------
				ELSIF v_actixExtent.SEGMENT_TYPE='LOBSEGMENT' AND v_actixExtent.SEGMENT_NAME LIKE 'SYS_LOB%' THEN
		  

					v_runpoint := 'SYS% segments - LOBSEGMENT - 1';
					BEGIN
						v_SQL := 'SELECT /*+ FIRST_ROWS(1) MONITOR */ TABLE_NAME
								  FROM ACTIX_LOBS
								  WHERE OWNER = :v_OWNER
								  AND SEGMENT_NAME = :v_SEGMENT_NAME
								  AND TABLESPACE_NAME = :v_TABLESPACE_NAME
								  AND ROWNUM = 1';

						EXECUTE IMMEDIATE v_SQL
						INTO v_TableName
						USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME, v_actixExtent.TABLESPACE_NAME;
						
					EXCEPTION
						WHEN OTHERS
							THEN
								v_SQL := 'SELECT /*+ FIRST_ROWS(1) MONITOR */ MAX(TABLE_NAME)
										  FROM DBA_LOBS
										  WHERE OWNER = :v_OWNER
										  AND SEGMENT_NAME = :v_SEGMENT_NAME
										  AND TABLESPACE_NAME = :v_TABLESPACE_NAME
										  AND ROWNUM = 1';
										  
								EXECUTE IMMEDIATE v_SQL
								INTO v_TableName
								USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME, v_actixExtent.TABLESPACE_NAME;
					END;


					IF v_TableName IS NOT NULL THEN
					
						SELECT REPLACE(MAX(COLUMN_NAME),'"','')
						INTO v_LOB_Column_Name
						FROM DBA_LOBS
						WHERE OWNER = v_actixExtent.OWNER
						AND SEGMENT_NAME= v_actixExtent.SEGMENT_NAME
						AND TABLE_NAME = v_TableName
						AND NOT EXISTS
									(
										SELECT NULL 
										FROM ALL_SDO_INDEX_METADATA
										WHERE SDO_INDEX_TABLE = v_TableName
									);

		  --dbms_output.PUT_LINE('Equivalent Segment found.');
						LOG(v_LogFileHandle,'Equivalent Segment: ' || v_actixExtent.OWNER || '.' || v_TableName);
						
	--
	-- LOBSEGMENT's equivalent not in ACTIX_SHRINK_EXTENTS
	--
						v_runpoint := 'SYS% segments - LOBSEGMENT - 2';
						v_SQL := 'UPDATE /*+ MONITOR */ ACTIX_SHRINK_EXTENTS
									SET SEGMENT_NAME = :v_TableName,' || CASE WHEN v_LOB_Column_Name IS NOT NULL THEN '
									IOT_SEGMENT_NAME = ''' || v_LOB_Column_Name || ''',' END || '
									SEGMENT_TYPE = ''TABLE''
									WHERE OWNER = :v_OWNER
									AND TABLESPACE_NAME = :v_TABLESPACE_NAME
									AND SEGMENT_NAME = :v_SEGMENT_NAME
									AND NOT EXISTS
												(
													SELECT NULL
													FROM ACTIX_SHRINK_EXTENTS
													WHERE SEGMENT_NAME = :v_TableName
													AND SEGMENT_TYPE = ''TABLE''
													AND OWNER = :v_OWNER
													AND TABLESPACE_NAME = :v_TABLESPACE_NAME
												)';

						EXECUTE IMMEDIATE v_SQL
						USING v_TableName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME, v_actixExtent.SEGMENT_NAME,
								v_TableName, v_actixExtent.OWNER, v_actixExtent.TABLESPACE_NAME;
								
						IF SQL%ROWCOUNT = 0 THEN
							LOG(v_LogFileHandle,'Equivalent Segment already exists in ACTIX_SHRINK_EXTENTS');

							v_SQL := 'DELETE FROM ACTIX_SHRINK_EXTENTS
										WHERE OWNER = :v_OWNER
										AND SEGMENT_NAME = :v_SEGMENT_NAME';
												
							EXECUTE IMMEDIATE v_SQL
							USING v_actixExtent.OWNER, v_actixExtent.SEGMENT_NAME;

							IF SQL%ROWCOUNT <> 0 THEN
								LOG(v_LogFileHandle,'Original Segment dropped from ACTIX_SHRINK_EXTENTS');
							END IF;
						END IF;
					ELSE 
						LOG(v_LogFileHandle,'Equivalent Segment not found');
					END IF;
				END IF;      
				
			END LOOP;
			COMMIT;
			LOG(v_LogFileHandle,'SYS Segment replacement complete');
			
		END REPLACE_SYS_SEGMENTS;
			
		PROCEDURE FLAG_IOTS
		IS
		BEGIN
			LOG(v_LogFileHandle,'Flagging IOTs in ACTIX_SHRINK_EXTENTS...');
			v_runpoint := 'Flagging the IOTs in ACTIX_SHRINK_EXTENTS';
			v_SQL := 'UPDATE /*+ MONITOR */ ACTIX_SHRINK_EXTENTS a
						SET IOT = 1,
						IOT_SEGMENT_NAME = (SELECT TABLE_NAME FROM ACTIX_IOTS WHERE CONSTRAINT_NAME = A.SEGMENT_NAME AND OWNER = A.OWNER)
						WHERE (OWNER, SEGMENT_NAME) IN
						  (
							SELECT OWNER, CONSTRAINT_NAME
							FROM ACTIX_IOTS
						  )
						OR (OWNER, SEGMENT_NAME) IN
						  (
							SELECT OWNER, TABLE_NAME
							FROM ACTIX_IOTS
						  )';
			
			EXECUTE IMMEDIATE v_SQL;
			
			IF SQL%ROWCOUNT <> 0 THEN
				LOG(v_LogFileHandle,TO_CHAR(SQL%ROWCOUNT) || ' IOT segments are flagged');
			ELSE
				LOG(v_LogFileHandle,'no IOT segments are flagged');
			END IF;
			
			COMMIT;
		END FLAG_IOTS;
			
		PROCEDURE GET_IOT_DEFINITIONS
		IS
		BEGIN
		
			LOG(v_LogFileHandle,'Retrieving IOT definitions...');
			DROP_TABLE('ACTIX_IOT_DEFINITIONS');
			
			v_runpoint := 'Creating ACTIX_IOT_DEFINITIONS';
			v_SQL := 'CREATE TABLE ACTIX_IOT_DEFINITIONS AS
						SELECT OWNER, SEGMENT_NAME, 
							REPLACE(REPLACE(REPLACE(Dbms_Metadata.Get_Ddl(''TABLE'' , NVL(IOT_SEGMENT_NAME,SEGMENT_NAME), OWNER),''"'',''''),NVL(IOT_SEGMENT_NAME,SEGMENT_NAME), ''ACTIX_IOT_TMP''),DECODE(IOT_SEGMENT_NAME, NULL, ''PK_'' || SEGMENT_NAME, SEGMENT_NAME) ,''PK_ACTIX_IOT_TMP'') IOT_DEFINITION
						FROM
						(
							SELECT distinct OWNER, SEGMENT_NAME, IOT_SEGMENT_NAME
							FROM ACTIX_SHRINK_EXTENTS
							WHERE IOT = 1
						)';
			EXECUTE IMMEDIATE v_SQL;
			LOG(v_LogFileHandle,'GET_IOT_DEFINITIONS complete!');
		END GET_IOT_DEFINITIONS;
		
		PROCEDURE CREATE_TABLE
		(
			p_TABLE_NAME		VARCHAR2,
			p_TS#				NUMBER DEFAULT NULL,
			p_SourceTablespace	VARCHAR2 DEFAULT NULL
		)
		IS
		BEGIN
			LOG(v_LogFileHandle,'Creating ' || CASE WHEN p_TABLE_NAME <> 'ACTIX_SHRINK_EXTENTS' THEN 'and populating' END || ' Table ' || p_TABLE_NAME || '...' );
			IF p_TABLE_NAME = 'ACTIX_SHRINK_EXTENTS' THEN
			
				DROP_TABLE('ACTIX_SHRINK_EXTENTS');
				v_runpoint := 'Creating empty ACTIX_SHRINK_EXTENTS Table';
				EXECUTE IMMEDIATE 'CREATE TABLE ACTIX_SHRINK_EXTENTS
									(
										OWNER               VARCHAR2(30),
										TABLESPACE_NAME     VARCHAR2(30),
										SEGMENT_TYPE        VARCHAR2(18),
										SEGMENT_NAME        VARCHAR2(81),
										PARTITION_NAME      VARCHAR2(30),
										BLOCK_ID            NUMBER,
										BLOCKS				NUMBER,
										IOT					NUMBER(1),
										IOT_SEGMENT_NAME	VARCHAR2(81)
									)  NOLOGGING TABLESPACE ACTIX_NOLOG_0001';
									
			ELSIF p_TABLE_NAME = 'DS' and v_DBA_Extent_Method = 1 THEN
			
				DROP_TABLE('DS');
				v_SQL := 'CREATE TABLE DS NOLOGGING TABLESPACE ACTIX_NOLOG_0001 AS 
			  select  NVL(u.name, ''SYS'') OWNER, o.name SEGMENT_NAME, o.subname PARTITION_NAME,
				   so.object_type SEGMENT_TYPE,  ts.ts# TABLESPACE_ID, ts.name TABLESPACE_NAME, ts.blocksize BLOCKSIZE,
				   s.block# HEADER_BLOCK, s.file# RELATIVE_FNO, NVL(s.spare1,0) SEGMENT_FLAGS
			from sys.user$ u, sys.obj$ o, sys.ts$ ts, sys.sys_objects so, sys.seg$ s,
				 sys.file$ f
			where s.file# = so.header_file
			  and s.block# = so.header_block
			  and s.ts# = so.ts_number
			  and s.ts# = ts.ts#
			  and o.obj# = so.object_id
			  and o.owner# = u.user# (+)
			  and s.type# = so.segment_type_id
			  and o.type# = so.object_type_id
			  and s.ts# = f.ts#
			  and s.file# = f.relfile#
			  and f.ts# = ' ||  TO_CHAR(p_TS#);

				v_runpoint := 'Creating DS Table';
				EXECUTE IMMEDIATE v_SQL;
				EXECUTE IMMEDIATE 'CREATE INDEX IX_DS ON DS(tablespace_id, bitand(NVL(segment_flags, 0), 1), bitand(NVL(segment_flags,0), 65536), owner) COMPRESS 3';
				DBMS_STATS.GATHER_TABLE_STATS(USER,'DS');
				DBMS_STATS.GATHER_INDEX_STATS(USER,'IX_DS');
				
			ELSIF p_TABLE_NAME = 'ACTIX_LOBS' THEN

				DROP_TABLE('ACTIX_LOBS');
				v_runpoint := 'Creating ACTIX_LOBS Table';
				v_SQL := 'CREATE TABLE ACTIX_LOBS NOLOGGING TABLESPACE ACTIX_NOLOG_0001 AS
							SELECT /*+ MONITOR */ * 
							FROM ALL_LOBS
							WHERE TABLESPACE_NAME =  ''' || p_SourceTablespace || '''
							OR (OWNER, INDEX_NAME) IN (
														SELECT /*+ NO_MERGE */ OWNER, SEGMENT_NAME
														FROM ACTIX_SHRINK_EXTENTS
														WHERE SEGMENT_TYPE = ''INDEX PARTITION''
														)
							OR (OWNER, SEGMENT_NAME) IN (
														SELECT /*+ NO_MERGE */ OWNER, SEGMENT_NAME
														FROM ACTIX_SHRINK_EXTENTS
														WHERE SEGMENT_TYPE = ''LOB PARTITION''
														)';
							
				EXECUTE IMMEDIATE v_SQL;
				DBMS_STATS.GATHER_TABLE_STATS(USER, 'ACTIX_LOBS');
				
			ELSIF p_TABLE_NAME = 'ACTIX_LOB_PARTITIONS' THEN
			
				DROP_TABLE('ACTIX_LOB_PARTITIONS');
				v_runpoint := 'Creating ACTIX_LOB_PARTITIONS Table';
				v_SQL := 'CREATE TABLE ACTIX_LOB_PARTITIONS NOLOGGING TABLESPACE ACTIX_NOLOG_0001 AS
							SELECT /*+ MONITOR */ * 
							FROM ALL_LOB_PARTITIONS
							WHERE TABLESPACE_NAME =  ''' || p_SourceTablespace || '''
							OR (TABLE_OWNER, LOB_NAME, LOB_PARTITION_NAME) IN (
																	SELECT /*+ NO_MERGE */ OWNER, SEGMENT_NAME, PARTITION_NAME
																	FROM ACTIX_SHRINK_EXTENTS
																	WHERE SEGMENT_TYPE = ''LOB PARTITION''
																)';
							
				EXECUTE IMMEDIATE v_SQL;
				DBMS_STATS.GATHER_TABLE_STATS(USER, 'ACTIX_LOB_PARTITIONS');
			
			ELSIF p_TABLE_NAME = 'ACTIX_IOTS' THEN
			
				DROP_TABLE('ACTIX_IOTS');
				v_runpoint := 'Creating ACTIX_IOTS Table';
				v_SQL := 'CREATE TABLE ACTIX_IOTS NOLOGGING TABLESPACE ACTIX_NOLOG_0001 AS
							SELECT /*+ MONITOR */ DISTINCT OWNER, TABLE_NAME, CONSTRAINT_NAME
							FROM ALL_CONSTRAINTS
							WHERE CONSTRAINT_TYPE=''P''
							AND (OWNER, TABLE_NAME) IN 
													(
														SELECT /*+ NO_MERGE */ OWNER, TABLE_NAME
														FROM ALL_TABLES
														WHERE IOT_TYPE = ''IOT''
														AND OWNER LIKE ''ACTIX%''
													)';
				EXECUTE IMMEDIATE v_SQL;
				DBMS_STATS.GATHER_TABLE_STATS(USER, 'ACTIX_IOTS');
				
			END IF;
			LOG(v_LogFileHandle,'Table ' || p_TABLE_NAME || ' Created' || CASE WHEN p_TABLE_NAME <> 'ACTIX_SHRINK_EXTENTS' THEN ' and populated' END );
		END CREATE_TABLE;
		
		PROCEDURE POPULATE_ACTIX_SHRINK_EXTENTS
		(
			p_TS#					NUMBER,
			p_SourceFileID			NUMBER,
			p_Threshold_BLOCK_ID	NUMBER DEFAULT NULL
		)
		IS
		BEGIN
			v_runpoint := 'Populating ACTIX_SHRINK_EXTENTS Table...';
			LOG(v_LogFileHandle,v_runpoint);
			IF v_DBA_Extent_Method = 1 THEN
				v_SQL := 'INSERT /*+ APPEND MONITOR */ INTO ACTIX_SHRINK_EXTENTS(owner, tablespace_name, segment_type, segment_name, partition_name, block_id, blocks)
						select owner, tablespace_name, segment_type, segment_name, partition_name, max(block_id) block_id, sum(blocks) blocks
						FROM
						(
						  select ds.owner, ds.segment_name, ds.partition_name, ds.segment_type,
								   ds.tablespace_name, f.file# FILE_ID, e.block# BLOCK_ID, e.length * ds.blocksize BYTES, e.length BLOCKS
							from sys.uet$ e,ds, sys.file$ f
							where e.segfile# = ds.relative_fno
							  and e.segblock# = ds.header_block
							  and e.ts# = ds.tablespace_id
							  and e.ts# = f.ts#
							  and e.file# = f.relfile#
							  and bitand(NVL(ds.segment_flags,0), 1) = 0
							  and bitand(NVL(ds.segment_flags,0), 65536) = 0
							and ds.tablespace_id = ' ||  TO_CHAR(p_TS#) || '
							union all
							select  /*+ monitor LEADING(f, ds, e) use_nl(ds e) use_index(e ind:1) */
								   ds.owner, ds.segment_name, ds.partition_name, ds.segment_type,
								   ds.tablespace_name, f.file# FILE_ID, e.ktfbuebno BLOCK_ID, e.ktfbueblks * ds.blocksize BYTES, e.ktfbueblks BLOCKS
							from sys.file$ f, ds, sys.x$ktfbue e
							where e.ktfbuesegfno = ds.relative_fno
							  and e.ktfbuesegbno = ds.header_block
							  and e.ktfbuesegtsn = ds.tablespace_id
							  and ds.tablespace_id = f.ts#
							  and e.ktfbuefno = f.relfile#
							  and bitand(NVL(ds.segment_flags, 0), 1) = 1
							  and bitand(NVL(ds.segment_flags,0), 65536) = 0
							and ds.tablespace_id = ' ||  TO_CHAR(p_TS#) || '
						)
						WHERE FILE_ID = ' || TO_CHAR(p_SourceFileID);
				ELSIF v_DBA_Extent_Method = 2 THEN
				v_SQL := 'INSERT /*+ APPEND MONITOR */ INTO ACTIX_SHRINK_EXTENTS(owner, tablespace_name, segment_type, segment_name, partition_name, block_id, blocks)
						select owner, tablespace_name, segment_type, segment_name, partition_name, max(block_id) block_id, sum(blocks) blocks
						FROM DBA_EXTENTS
						WHERE FILE_ID = ' || TO_CHAR(p_SourceFileID);
				END IF;
				IF p_Threshold_BLOCK_ID <> 0 THEN
					v_SQL := v_SQL || ' AND BLOCK_ID > ' || TO_CHAR(NVL(p_Threshold_BLOCK_ID,0)) || ' ';
				END IF;
				v_SQL := v_SQL || ' group by owner, tablespace_name, segment_type, segment_name, partition_name';
					
			EXECUTE IMMEDIATE v_SQL;
			COMMIT;
			
			IF SQL%ROWCOUNT = 0 AND v_DBA_Extent_Method = 1 THEN
				v_SQL := 'INSERT /*+ APPEND MONITOR */ INTO ACTIX_SHRINK_EXTENTS(owner, tablespace_name, segment_type, segment_name, partition_name, block_id, blocks)
						SELECT owner, tablespace_name, segment_type, segment_name, partition_name, MAX(block_id), SUM(blocks)
						FROM DBA_EXTENTS
						WHERE FILE_ID = ' || TO_CHAR(p_SourceFileID);
				IF p_Threshold_BLOCK_ID <> 0 THEN
					v_SQL := v_SQL || 'AND BLOCK_ID > ' || TO_CHAR(NVL(p_Threshold_BLOCK_ID,0)) || ' ';
				END IF;
				v_SQL := v_SQL || ' group by owner, tablespace_name, segment_type, segment_name, partition_name';
						
				EXECUTE IMMEDIATE v_SQL;
				COMMIT;
			END IF;
			
			DBMS_STATS.GATHER_TABLE_STATS(USER, 'ACTIX_SHRINK_EXTENTS');
			
			LOG(v_LogFileHandle,'Table ACTIX_SHRINK_EXTENTS is populated');
		
		END POPULATE_ACTIX_SHRINK_EXTENTS;
		
	BEGIN
	
		CREATE_TABLE('ACTIX_SHRINK_EXTENTS');
		
		CREATE_TABLE('DS', v_TS#);

		POPULATE_ACTIX_SHRINK_EXTENTS(v_TS#, v_SourceFileID, v_Threshold_BLOCK_ID);
		
		CREATE_TABLE ('ACTIX_LOBS', p_SourceTablespace => v_SourceTablespace);
		
		CREATE_TABLE('ACTIX_LOB_PARTITIONS', p_SourceTablespace => v_SourceTablespace);

		CREATE_TABLE('ACTIX_IOTS');
--
-- Replacing SYS% segments with equivalents
--
		REPLACE_SYS_SEGMENTS;
		
--
-- Flagging the IOTs in ACTIX_SHRINK_EXTENTS
--
		FLAG_IOTS;

--
-- Creating ACTIX_IOT_DEFINITIONS
--
		GET_IOT_DEFINITIONS;
		
		CALCULATE_PARALLEL_BLK_LIMIT;
	END Create_Segment_list;
	
	PROCEDURE CHANGE_DEFAULT_TABLESPACE
	(
		p_Type	IN VARCHAR2
	)
	IS
	BEGIN
		LOG(v_LogFileHandle,'Changing ' || p_Type || ' segments'' default tablespace...');

		IF p_Type = 'TABLE' THEN
			v_runpoint := 'Changing default tablespace for Composite partitioned tables';
			v_SQL := 'SELECT /*+ MONITOR */ ''ALTER TABLE '' || TABLE_OWNER || ''.'' || TABLE_NAME || '' MODIFY DEFAULT ATTRIBUTES FOR PARTITION '' || PARTITION_NAME || '' TABLESPACE ' || v_DestinationTablespace || ''' 
						FROM ALL_TAB_PARTITIONS ATP
						WHERE TABLESPACE_NAME = ''' || v_SourceTablespace || ''' 
						AND COMPOSITE =''YES''
						AND EXISTS (SELECT NULL
									FROM ACTIX_SHRINK_EXTENTS
									WHERE OWNER = TABLE_OWNER
									AND SEGMENT_NAME = TABLE_NAME
									AND PARTITION_NAME = ATP.PARTITION_NAME)';
			OPEN C_Cursor FOR v_SQL;
			LOOP
				FETCH C_Cursor INTO v_statement;
				EXIT WHEN C_Cursor%NOTFOUND;

				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_statement);
				
			END LOOP;
			
		ELSIF p_Type = 'INDEX' THEN
		
			v_runpoint := 'Changing default tablespace for Composite partitioned indexs';
			v_SQL := 'SELECT /*+ MONITOR */ ''ALTER INDEX '' || INDEX_OWNER || ''.'' || INDEX_NAME || '' MODIFY DEFAULT ATTRIBUTES FOR PARTITION '' || PARTITION_NAME || '' TABLESPACE ' || v_DestinationTablespace || ''' 
						FROM ALL_IND_PARTITIONS AIP
						WHERE TABLESPACE_NAME = ''' || v_SourceTablespace || ''' 
						AND COMPOSITE =''YES''
						AND EXISTS (SELECT NULL
									FROM ACTIX_SHRINK_EXTENTS
									WHERE OWNER = INDEX_OWNER
									AND SEGMENT_NAME = INDEX_NAME
									AND PARTITION_NAME = AIP.PARTITION_NAME)';
			OPEN C_Cursor FOR v_SQL;
			LOOP
				FETCH C_Cursor INTO v_statement;
				EXIT WHEN C_Cursor%NOTFOUND;

				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_statement);
			END LOOP;
			
		END IF;
	END CHANGE_DEFAULT_TABLESPACE;
	
	PROCEDURE MOVE_REMAINING_SEGMENTS
	(
		p_Type	IN VARCHAR2
	)
	IS
	BEGIN
		LOG(v_LogFileHandle,'Moving remaining ' || p_Type || ' segments...');
		IF p_Type = 'TABLE' THEN
			v_runpoint := 'Rebuilding remaining table [sub]partitions in ' || v_SourceTablespace;
			v_SQL := 'SELECT /*+ MONITOR */ ''ALTER TABLE '' || TABLE_OWNER || ''.'' || TABLE_NAME || '' MOVE '' || CASE WHEN SUBPARTITION_NAME IS NOT NULL THEN ''SUB'' END  || ''PARTITION '' || CASE WHEN SUBPARTITION_NAME IS NOT NULL THEN ''SUB'' WHEN PARTITION_NAME IS NOT NULL THEN PARTITION_NAME END || '' TABLESPACE ' || v_DestinationTablespace || ' '' 
						FROM
						(
							SELECT TABLE_OWNER, TABLE_NAME, PARTITION_NAME, NULL SUBPARTITION_NAME, TABLESPACE_NAME
							FROM DBA_TAB_PARTITIONS
							WHERE COMPOSITE = ''NO''
							UNION ALL
							SELECT TABLE_OWNER, TABLE_NAME, PARTITION_NAME, SUBPARTITION_NAME, TABLESPACE_NAME
							FROM DBA_TAB_SUBPARTITIONS
						) T
						WHERE TABLESPACE_NAME = ''' || v_SourceTablespace || ''' 
						AND EXISTS (SELECT NULL
									FROM ACTIX_SHRINK_EXTENTS
									WHERE OWNER = TABLE_OWNER
									AND SEGMENT_NAME = TABLE_NAME
									AND PARTITION_NAME = NVL(SUBPARTITION_NAME, T.PARTITION_NAME))';
			OPEN C_Cursor FOR v_SQL;
			LOOP
				FETCH C_Cursor INTO v_statement;
				EXIT WHEN C_Cursor%NOTFOUND;

				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_statement);
				
			END LOOP;

		ELSIF p_Type = 'INDEX' THEN
			v_runpoint := 'Rebuilding remaining index [sub]partitions in ' || v_SourceTablespace;
			v_SQL := 'SELECT /*+ MONITOR */ ''ALTER INDEX '' || INDEX_OWNER || ''.'' || INDEX_NAME || '' REBUILD '' || CASE WHEN SUBPARTITION_NAME IS NOT NULL THEN ''SUB'' END  || ''PARTITION '' || CASE WHEN SUBPARTITION_NAME IS NOT NULL THEN ''SUB'' WHEN PARTITION_NAME IS NOT NULL THEN PARTITION_NAME END || '' TABLESPACE ' || v_DestinationTablespace || ' '' 
						FROM
						(
							SELECT INDEX_OWNER, INDEX_NAME, PARTITION_NAME, NULL SUBPARTITION_NAME, TABLESPACE_NAME
							FROM DBA_IND_PARTITIONS
							WHERE COMPOSITE = ''NO''
							UNION ALL
							SELECT INDEX_OWNER, INDEX_NAME, PARTITION_NAME, SUBPARTITION_NAME, TABLESPACE_NAME
							FROM DBA_IND_SUBPARTITIONS
						) I
						WHERE TABLESPACE_NAME = ''' || v_SourceTablespace || ''' 
						AND EXISTS (SELECT NULL
									FROM ACTIX_SHRINK_EXTENTS
									WHERE OWNER = INDEX_OWNER
									AND SEGMENT_NAME = INDEX_NAME
									AND PARTITION_NAME = NVL(SUBPARTITION_NAME, I.PARTITION_NAME))';
			OPEN C_Cursor FOR v_SQL;
			LOOP
				FETCH C_Cursor INTO v_statement;
				EXIT WHEN C_Cursor%NOTFOUND;

				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_statement);
				
			END LOOP;
		END IF;
	END MOVE_REMAINING_SEGMENTS;

	PROCEDURE GRANT_TABLESPACE_QUOTA
	IS
		v_Owner	VARCHAR2(30);
	BEGIN
		v_SQL := 'SELECT /*+ PARALLEL(4) */ DISTINCT OWNER FROM ACTIX_SHRINK_EXTENTS';
		OPEN C_Cursor FOR v_SQL;
		LOOP
			FETCH C_Cursor INTO v_Owner;
			EXIT WHEN C_Cursor%NOTFOUND;
			BEGIN
				EXECUTE IMMEDIATE 'ALTER USER ' || v_Owner ||  ' QUOTA UNLIMITED ON ' || v_DestinationTablespace;
			EXCEPTION
				WHEN OTHERS THEN
			LOG(v_LogFileHandle,'v_runpoint:' || v_runpoint);
			LOG(v_LogFileHandle,'SQLERRM:' || SQLERRM);
			LOG(v_LogFileHandle,'ALTER USER ' || v_Owner ||  ' QUOTA UNLIMITED ON ' || v_DestinationTablespace);
			END;
			
		END LOOP;
		CLOSE C_Cursor;
	END GRANT_TABLESPACE_QUOTA;

	
	PROCEDURE move_segment
	(
		p_Segment			SegmentRow,
		p_DestTablespace	VARCHAR2,
		p_moved				OUT NUMBER
	)
	IS
	
		v_Segment			SegmentRow := p_Segment;
		v_SegmentName		VARCHAR2(30);
		v_SegmentType		VARCHAR2(30);
		v_IndexType			VARCHAR2(30);
		v_TableName			VARCHAR2(30);
		v_LOBName			VARCHAR2(30);
		v_PartitionName		VARCHAR2(30);
		v_SpatialIndex		VARCHAR2(30);
		v_LOBColumn			VARCHAR2(30);
		v_IOTOverFlow		VARCHAR2(50);
		v_SpatialIndexPart	VARCHAR2(30);
		v_NewBlockID		NUMBER;
		v_MaxHeaderBlock	NUMBER;
		v_IOT				NUMBER(1);
		v_CNT				NUMBER;
		v_AlteredTablespace	VARCHAR2(32);
		V_SDO_INDEX_NAME 	VARCHAR2(32);
		v_SDO_INDEX_PARTITION VARCHAR2(32);
		v_runpoint			varchar2(300) := 'Starting move_segment';
		v_CreateTempIOTSegment CLOB;
		v_DropTempIOTSegment	VARCHAR2(100);
		v_ParallelClause	VARCHAR2(30);
		v_HintParallelClause	VARCHAR2(30);
		v_PKName				VARCHAR2(30);
		v_SDO_Index_Parameters	VARCHAR2(1000);
		
		FUNCTION REPLACE_MDRT_SEGMENT
		(
			p_OWNER					VARCHAR2,
			p_SEGMENT_NAME			VARCHAR2,
			p_DestinationTablespace	VARCHAR2,
			p_ParallelClause		VARCHAR2 DEFAULT NULL
		)
		RETURN VARCHAR2
		IS
			v_TableName				VARCHAR2(30);
			V_SDO_INDEX_NAME 		VARCHAR2(32);
			v_SDO_INDEX_PARTITION 	VARCHAR2(32);
			v_SDO_Index_Parameters	VARCHAR2(1000);
			v_SQL					varchar2(2250);
			
			FUNCTION REMOVE_TABLESPACE_DEFINITION
			(
				p_SDO_Index_Parameters	VARCHAR2
			)
			RETURN VARCHAR2
			IS
				v_SDO_Index_Parameters	VARCHAR2(1000) := p_SDO_Index_Parameters;
				v_No_Tablespace_Parameters	VARCHAR2(1000);
				l_Parameters_Count			PLS_INTEGER := 1;
				TYPE t_Parameter_Array IS TABLE OF VARCHAR2(200) INDEX BY PLS_INTEGER;
				l_Parameter_Array			t_Parameter_Array;	
			BEGIN
				LOOP
					IF INSTR(v_SDO_Index_Parameters,',') <> 0 THEN
						l_Parameter_Array(l_Parameters_Count) := SUBSTR(v_SDO_Index_Parameters, 1, INSTR(v_SDO_Index_Parameters, ',') - 1);
						v_SDO_Index_Parameters := LTRIM(SUBSTR(REPLACE(v_SDO_Index_Parameters,l_Parameter_Array(l_Parameters_Count),''),2),',');
						l_Parameters_Count := l_Parameters_Count + 1;
					ELSE
						l_Parameter_Array(l_Parameters_Count) := RTRIM(v_SDO_Index_Parameters,' ');
						EXIT;
					END IF;
				END LOOP;
				FOR l_parameter_no IN 1..l_Parameters_Count LOOP
					IF LOWER(l_Parameter_Array(l_parameter_no)) NOT LIKE 'tablespace=%' THEN
						v_No_Tablespace_Parameters := v_No_Tablespace_Parameters || CASE WHEN v_No_Tablespace_Parameters IS NOT NULL THEN ',' END ||  LOWER(l_Parameter_Array(l_parameter_no));
					END IF;
				END LOOP;
				RETURN v_No_Tablespace_Parameters;
			END REMOVE_TABLESPACE_DEFINITION;
		BEGIN
			v_runpoint := 'Moving MDRT%$ TABLE';
			SELECT MAX(SDO_INDEX_NAME), MAX(SDO_INDEX_PARTITION)
			INTO v_SDO_INDEX_NAME, v_SDO_INDEX_PARTITION
			FROM ALL_SDO_INDEX_METADATA
			WHERE SDO_INDEX_TABLE = p_SEGMENT_NAME;
			
			IF v_SDO_INDEX_NAME IS NOT NULL AND v_SDO_INDEX_PARTITION IS NOT NULL THEN 
				v_runpoint := 'REPLACE_MDRT_SEGMENT - Getting Index Partition PARAMETERS';
				LOG(v_LogFileHandle,v_runpoint);
				
				SELECT RTRIM(MAX(PARAMETERS))
				INTO v_SDO_Index_Parameters
				FROM DBA_IND_PARTITIONS
				WHERE INDEX_OWNER = p_OWNER
				AND INDEX_NAME = v_SDO_INDEX_NAME
				AND PARTITION_NAME = v_SDO_INDEX_PARTITION;
				
				v_SQL := 'ALTER INDEX ' || p_OWNER || '.' || v_SDO_INDEX_NAME || ' REBUILD PARTITION ' || v_SDO_INDEX_PARTITION || ' PARAMETERS(''tablespace=' || p_DestinationTablespace || ',' || REMOVE_TABLESPACE_DEFINITION(v_SDO_Index_Parameters) || ''')'  || p_ParallelClause;
			END IF;
			RETURN v_SQL;
		END REPLACE_MDRT_SEGMENT;
		
	BEGIN
		--dbms_output.put_line(v_runpoint);
		v_runpoint := 'Deciding the Parallel Degree';
		IF v_Segment.BLOCKS >= v_ParallelThreshold THEN
			v_ParallelClause := ' PARALLEL 4';
			v_HintParallelClause := ' PARALLEL(4)';
		END IF;
		
		v_runpoint := 'IOT or other kind of segment?';
		IF NVL(v_Segment.IOT,0) = 1 AND v_Segment.PARTITION_NAME IS NOT NULL THEN
		
			v_runpoint := 'Dropping existing ACTIX_IOT_TMP';
			SELECT /*+ FIRST_ROWS(1) */ COUNT(*)
			INTO v_CNT
			FROM ALL_OBJECTS
			WHERE OWNER = v_Segment.OWNER
			AND OBJECT_TYPE = 'TABLE'
			AND OBJECT_NAME = 'ACTIX_IOT_TMP'
			AND ROWNUM = 1;
			
			IF v_CNT <> 0 THEN
				EXECUTE IMMEDIATE 'DROP TABLE ' || v_Segment.OWNER || '.ACTIX_IOT_TMP PURGE';
			END IF;

			v_runpoint := 'Getting Temp IOT DDL Statement';
			v_SQL := 'SELECT IOT_DEFINITION
						FROM ACTIX_IOT_DEFINITIONS
						WHERE OWNER = :v_OWNER
						AND SEGMENT_NAME = :v_SEGMENT_NAME';
			EXECUTE IMMEDIATE v_SQL 
			INTO v_CreateTempIOTSegment
			USING v_Segment.OWNER, v_Segment.SEGMENT_NAME;
			
			v_CreateTempIOTSegment := v_CreateTempIOTSegment || ' TABLESPACE ' || NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || v_ParallelClause ;

			v_runpoint := 'Creating Temp IOT Segment';
			EXECUTE IMMEDIATE v_CreateTempIOTSegment;

			v_SQL := 'INSERT /*+ APPEND' || v_HintParallelClause || ' */ INTO ' || v_Segment.OWNER || '.ACTIX_IOT_TMP
			SELECT /*+' || v_HintParallelClause || ' MONITOR */ * FROM ' || v_Segment.OWNER || '.' || NVL(v_Segment.IOT_SEGMENT_NAME,v_Segment.SEGMENT_NAME) || ' PARTITION(' || v_Segment.PARTITION_NAME || ')';
			EXECUTE IMMEDIATE v_SQL;

			COMMIT;
			
			IF v_Segment.BLOCKS >= v_ParallelThreshold THEN
				EXECUTE IMMEDIATE 'ALTER TABLE ' || v_Segment.OWNER || '.ACTIX_IOT_TMP NOPARALLEL';
			END IF;
			
			v_runpoint := 'Disabling IOT''s FK Constraints';
			FOR l_disableConstraint IN (select 'ALTER TABLE ' || v_Segment.OWNER || '.' || NVL(v_Segment.IOT_SEGMENT_NAME,v_Segment.SEGMENT_NAME) || ' DISABLE CONSTRAINT ' || CONSTRAINT_NAME Statement
										from DBA_CONSTRAINTS
										where OWNER = v_Segment.OWNER
										and TABLE_NAME = NVL(v_Segment.IOT_SEGMENT_NAME,v_Segment.SEGMENT_NAME)
										and CONSTRAINT_TYPE = 'R') LOOP
				
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, l_disableConstraint.Statement);
				--dbms_output.put_line(l_disableConstraint.Statement);
			
			END LOOP;
			
			v_runpoint := 'Switching IOT''s Partition with Temp IOT Segment';
			v_SQL := 'ALTER TABLE ' || v_Segment.OWNER || '.' || NVL(v_Segment.IOT_SEGMENT_NAME,v_Segment.SEGMENT_NAME) || ' EXCHANGE PARTITION ' || v_Segment.PARTITION_NAME || ' WITH TABLE ' || v_Segment.OWNER || '.ACTIX_IOT_TMP';
			EXECUTE IMMEDIATE v_SQL;
			--dbms_output.put_line('carried out v_SQL:' || v_SQL);
			
			v_runpoint := 'Enabling IOT''s FK Constraints';
			FOR l_enableConstraint IN (select 'ALTER TABLE ' || v_Segment.OWNER || '.' || NVL(v_Segment.IOT_SEGMENT_NAME,v_Segment.SEGMENT_NAME) || ' ENABLE NOVALIDATE CONSTRAINT ' || CONSTRAINT_NAME Statement
										from DBA_CONSTRAINTS
										where OWNER = v_Segment.OWNER
										and TABLE_NAME = NVL(v_Segment.IOT_SEGMENT_NAME,v_Segment.SEGMENT_NAME)
										and CONSTRAINT_TYPE = 'R') LOOP
				
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, l_enableConstraint.Statement);
				--dbms_output.put_line(l_enableConstraint.Statement);
			
			END LOOP;
			
			v_runpoint := 'Dropping Temp IOT Segment';
			v_DropTempIOTSegment := 'DROP TABLE ' || v_Segment.OWNER || '.ACTIX_IOT_TMP PURGE';
			EXECUTE IMMEDIATE v_DropTempIOTSegment;
			--dbms_output.put_line('dropped ACTIX_IOT_TMP');
			
			
		ELSIF v_Segment.SEGMENT_TYPE IN ('INDEX PARTITION','TABLE PARTITION','INDEX SUBPARTITION','TABLE SUBPARTITION') THEN

			IF v_Segment.SEGMENT_NAME NOT LIKE 'PK%' AND v_Segment.SEGMENT_NAME NOT LIKE 'SYS_IL%' AND v_Segment.PARTITION_NAME NOT LIKE 'SYS_IL%' THEN

				v_runpoint := 'Moving Normal Partition/Subpartition';
				v_SQL := 'ALTER '|| SUBSTR(v_Segment.SEGMENT_TYPE,1,5) || ' ' || v_Segment.OWNER || '.' || v_Segment.SEGMENT_NAME || ' ' || CASE WHEN SUBSTR(v_Segment.SEGMENT_TYPE,1,5) = 'INDEX' THEN 'REBUILD ' ELSE 'MOVE ' END || SUBSTR(v_Segment.SEGMENT_TYPE,INSTR(v_Segment.SEGMENT_TYPE,' ')+1,LENGTH(v_Segment.SEGMENT_TYPE)-INSTR(v_Segment.SEGMENT_TYPE,' ')) || ' ' || v_Segment.PARTITION_NAME || ' TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || CASE WHEN v_Segment.SEGMENT_TYPE = 'TABLE PARTITION' AND SUBSTR(v_Segment.PARTITION_NAME,1,3) <> 'SYS' THEN ' NOLOGGING' || CASE WHEN v_Segment.IOT IS NULL THEN v_ParallelClause END  END;
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);

			ELSIF v_Segment.SEGMENT_NAME LIKE 'PK%' THEN

				v_runpoint := 'Moving Primary Key';

				SELECT MAX(INDEX_TYPE), MAX(TABLE_NAME)
				INTO v_IndexType, v_TableName
				FROM
				(
					SELECT OWNER, INDEX_NAME, INDEX_TYPE, TABLE_NAME
					FROM ALL_INDEXES
					JOIN ALL_IND_PARTITIONS USING(INDEX_NAME)
					WHERE PARTITION_NAME=v_Segment.PARTITION_NAME
					UNION ALL
					SELECT OWNER, INDEX_NAME, INDEX_TYPE, TABLE_NAME
					FROM ALL_INDEXES
					JOIN ALL_IND_SUBPARTITIONS USING(INDEX_NAME)
					WHERE SUBPARTITION_NAME=v_Segment.PARTITION_NAME
				)
				WHERE INDEX_NAME=v_Segment.SEGMENT_NAME
				AND OWNER=v_Segment.Owner;

				IF NVL(v_IndexType,'NULL') = 'IOT - TOP' THEN

					v_runpoint := 'Moving IOT';
					
					v_SQL := 'ALTER TABLE ' || v_Segment.Owner || '.' || v_TableName || ' MOVE PARTITION ' || v_Segment.PARTITION_NAME || ' TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ' NOLOGGING';
					RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
					
				ELSIF v_IndexType IS NOT NULL THEN

					v_runpoint := 'Moving Normal Primary Key';
					IF v_Segment.SEGMENT_TYPE = 'INDEX PARTITION' THEN
					
						v_SQL := 'ALTER INDEX ' || v_Segment.Owner || '.' || v_Segment.SEGMENT_NAME || ' REBUILD PARTITION ' || v_Segment.PARTITION_NAME  || ' TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ' NOLOGGING' || v_ParallelClause;
						
					ELSIF v_Segment.SEGMENT_TYPE = 'INDEX SUBPARTITION' THEN
					
						v_SQL := 'ALTER INDEX ' || v_Segment.Owner || '.' || v_Segment.SEGMENT_NAME || ' REBUILD SUBPARTITION ' || v_Segment.PARTITION_NAME  || ' TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME);
						
					END IF;
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
				
				END IF;
				
			ELSIF v_Segment.SEGMENT_NAME LIKE 'SYS_IL%' AND v_Segment.PARTITION_NAME LIKE 'SYS_IL%' THEN

				v_runpoint := 'Getting info for SYS_IL%(SYS_IL%) LOB';

				SELECT MAX(TABLE_NAME), MAX(PARTITION_NAME), MAX(COLUMN_NAME)
				INTO v_TableName, v_PartitionName, v_LOBColumn
				FROM
				(
					SELECT dlp.TABLE_NAME, dlp.PARTITION_NAME, dl.COLUMN_NAME
					FROM DBA_LOBS dl
					JOIN DBA_LOB_PARTITIONS dlp
						ON (
							dl.TABLE_NAME = dlp.TABLE_NAME
							AND dl.SEGMENT_NAME = dlp.LOB_NAME
							AND dl.OWNER = dlp.TABLE_OWNER
							)
					WHERE dl.INDEX_NAME = v_Segment.SEGMENT_NAME
						AND dl.OWNER = v_Segment.Owner
						AND dlp.LOB_INDPART_NAME = v_Segment.PARTITION_NAME
						AND dlp.TABLESPACE_NAME <> NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME)
				GROUP BY dlp.TABLE_NAME, dlp.PARTITION_NAME, dl.COLUMN_NAME);

				IF v_TableName IS NOT NULL THEN

					IF INSTR(v_LOBColumn,'.') <> 0 THEN
					
						v_runpoint := 'Moving for SYS_IL%(SYS_IL%) LOB partition';
						v_SQL := 'ALTER TABLE '||v_Segment.Owner||'.'|| v_TableName ||' MOVE PARTITION '|| v_PartitionName || ' TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ' NOLOGGING' || v_ParallelClause;
						RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
						
					ELSE
					
						SELECT DECODE(IOT_TYPE,'IOT',1,0)
						INTO v_IOT
						FROM DBA_TABLES
						WHERE OWNER = v_Segment.OWNER
						AND TABLE_NAME = v_TableName;
						
						v_runpoint := 'Moving for SYS_IL%(SYS_IL%) LOB in partition';
						v_SQL := 'ALTER TABLE '||v_Segment.Owner||'.'|| v_TableName ||' MOVE PARTITION '|| v_PartitionName ||' LOB (' || v_LOBColumn || ') STORE AS (TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ') NOLOGGING' || CASE WHEN v_IOT = 0 THEN v_ParallelClause END;
						RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
					END IF;
				END IF;
			END IF;

		ELSIF v_Segment.SEGMENT_TYPE='LOB PARTITION' THEN

			SELECT MAX(TABLE_NAME), MAX(PARTITION_NAME), MAX(COLUMN_NAME)
			INTO v_TableName, v_PartitionName,v_LOBColumn
			FROM DBA_LOB_PARTITIONS
			WHERE TABLESPACE_NAME <> NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME)
			AND LOB_NAME=v_Segment.SEGMENT_NAME
			AND LOB_PARTITION_NAME=v_Segment.PARTITION_NAME
			AND TABLE_OWNER=v_Segment.OWNER;

			IF v_TableName IS NOT NULL THEN
				
				SELECT DECODE(IOT_TYPE,'IOT',1,0)
				INTO v_IOT
				FROM DBA_TABLES
				WHERE OWNER = v_Segment.OWNER
				AND TABLE_NAME = v_TableName;
				
				v_runpoint := 'Moving LOB PARTITION';
				v_SQL := 'ALTER TABLE '||v_Segment.OWNER||'.'|| v_TableName ||' MOVE PARTITION '|| v_PartitionName || ' TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ' NOLOGGING' || CASE WHEN v_IOT = 0 THEN v_ParallelClause END;
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);

				IF INSTR(v_LOBColumn,'.') = 0 THEN
				
					v_runpoint := 'Moving for SYS_IL%(SYS_IL%) LOB in partition';
					v_SQL := 'ALTER TABLE '||v_Segment.Owner||'.'|| v_TableName ||' MOVE PARTITION '|| v_PartitionName ||' LOB (' || v_LOBColumn || ') STORE AS (TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ') NOLOGGING' || CASE WHEN v_IOT = 0 THEN v_ParallelClause END;
					RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
					
				END IF;

			END IF;

		ELSIF v_Segment.SEGMENT_TYPE='TABLE' THEN

			v_runpoint := 'Moving TABLE';
			v_SQL := 'ALTER TABLE '||v_Segment.OWNER||'.'|| v_Segment.SEGMENT_NAME ||' MOVE TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ' NOLOGGING' || v_ParallelClause;
			RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
				
			IF v_Segment.SEGMENT_NAME LIKE 'MDRT%$' AND v_Segment.IOT_SEGMENT_NAME IS NULL THEN

				v_runpoint := 'Moving MDRT%$ TABLE';
				v_SQL := REPLACE_MDRT_SEGMENT(v_Segment.OWNER, v_Segment.SEGMENT_NAME, p_DestTablespace, v_ParallelClause);			
				
				IF v_SQL IS NOT NULL THEN 
					v_runpoint := 'Moving MDRT%$ TABLE - Getting Index Partition PARAMETERS';
					LOG(v_LogFileHandle,v_runpoint);
					LOG(v_LogFileHandle,v_SQL);
					RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
				ELSE 
					LOG(v_LogFileHandle,'The equivalent segment for the MDRT table not found.');
				END IF;
				
			ELSIF v_Segment.SEGMENT_NAME LIKE 'MDRT%$' AND v_Segment.IOT_SEGMENT_NAME IS NOT NULL THEN
			
				v_runpoint := 'Moving MDRT%$ TABLE - IOT';
				v_SQL := 'ALTER TABLE '|| v_Segment.OWNER || '.' || v_Segment.SEGMENT_NAME || ' MOVE LOB (' || v_Segment.IOT_SEGMENT_NAME || ') STORE AS (TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ') NOLOGGING' || v_ParallelClause;
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
				
			END IF;
		ELSIF v_Segment.SEGMENT_TYPE='LOBINDEX' AND v_Segment.SEGMENT_NAME LIKE 'SYS_IL%' THEN

			v_runpoint := 'Retrieving Table_Name for LOBINDEX(SYS_IL%)';

			SELECT MAX(TABLE_NAME), MAX(COLUMN_NAME)
			INTO v_TableName, v_LOBColumn
			FROM DBA_LOBS
			WHERE OWNER = v_Segment.OWNER
			AND INDEX_NAME=v_Segment.SEGMENT_NAME
			AND TABLESPACE_NAME <> NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME);

			IF v_TableName IS NOT NULL THEN

				SELECT DECODE(IOT_TYPE,'IOT',1,0)
				INTO v_IOT
				FROM DBA_TABLES
				WHERE OWNER = v_Segment.OWNER
				AND TABLE_NAME = v_TableName;
				
				v_runpoint := 'Moving for LOBINDEX(SYS_IL%)';
				v_SQL := 'ALTER TABLE '||v_Segment.OWNER||'.'|| v_TableName ||' MOVE TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ' NOLOGGING' || CASE WHEN v_IOT = 0 THEN v_ParallelClause END;
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);

				IF v_TableName LIKE 'MDRT%$' THEN

					v_runpoint := 'Moving rebuilding associated Spatial Index for LOBINDEX(SYS_IL%)';
					SELECT MAX(SDO_INDEX_NAME), MAX(SDO_INDEX_PARTITION)
					INTO v_SDO_INDEX_NAME, v_SDO_INDEX_PARTITION
					FROM ALL_SDO_INDEX_METADATA
					WHERE SDO_INDEX_TABLE = v_TableName;

					IF v_SDO_INDEX_NAME IS NULL THEN 
					
						v_SQL := 'ALTER TABLE '|| v_Segment.OWNER || '.' || v_TableName || ' MOVE LOB (' || v_LOBColumn || ') STORE AS (TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ') NOLOGGING' || CASE WHEN v_IOT = 0 THEN v_ParallelClause END;
						
					ELSE
					
						v_SQL := 'ALTER INDEX ' || v_Segment.OWNER || '.' || v_SDO_INDEX_NAME || ' REBUILD PARTITION ' || v_SDO_INDEX_PARTITION || v_ParallelClause;
						
					END IF;
						RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
				END IF;
			END IF;

		ELSIF v_Segment.SEGMENT_TYPE='LOBSEGMENT' AND v_Segment.SEGMENT_NAME LIKE 'SYS_LOB%' THEN

			v_runpoint := 'Retrieving Table_Name/Column_Name for LOBSEGMENT(SYS_LOB%)';
			SELECT MAX(TABLE_NAME), MAX(COLUMN_NAME), MAX(SEG_TYPE)
			INTO v_TableName, v_LOBColumn, v_SegmentType
			FROM
			(
				SELECT TABLE_NAME, COLUMN_NAME, (SELECT SEGMENT_TYPE FROM SYS_DBA_SEGS WHERE SEGMENT_NAME = L.TABLE_NAME GROUP BY SEGMENT_TYPE) SEG_TYPE
				FROM DBA_LOBS l
				WHERE OWNER = v_Segment.OWNER
				AND SEGMENT_NAME = v_Segment.SEGMENT_NAME
				AND TABLESPACE_NAME <> NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME)
				GROUP BY TABLE_NAME, COLUMN_NAME
			);

			IF v_TableName IS NOT NULL AND v_SegmentType = 'TABLE' AND v_Segment.PARTITION_NAME IS NULL THEN
			
				SELECT DECODE(IOT_TYPE,'IOT',1,0)
				INTO v_IOT
				FROM DBA_TABLES
				WHERE OWNER = v_Segment.OWNER
				AND TABLE_NAME = v_TableName;
				
				v_runpoint := 'Moving LOBSEGMENT(SYS_LOB%)';
				v_SQL := 'ALTER TABLE '||v_Segment.OWNER||'.'||v_TableName||' MOVE LOB (' || v_LOBColumn || ') STORE AS (TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ') NOLOGGING' || CASE WHEN v_IOT = 0 THEN v_ParallelClause END;
				RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);
			END IF;

		ELSIF v_Segment.SEGMENT_TYPE='INDEX' AND v_Segment.SEGMENT_NAME LIKE 'PK%' THEN

			v_runpoint := 'Gathering info for Primary Key of non-partitioned table';
			SELECT MAX(INDEX_TYPE), MAX(TABLE_NAME),MAX(OVERFLOW_SEGMENT)
			INTO v_IndexType, v_TableName, v_IOTOverFlow
			FROM
			(
				SELECT I.INDEX_TYPE, I.TABLE_NAME,CASE WHEN IOF.INDEX_NAME LIKE 'SYS_IOT%' THEN IOF.INDEX_NAME ELSE NULL END OVERFLOW_SEGMENT
				FROM ALL_INDEXES I
				LEFT OUTER JOIN ALL_INDEXES IOF 
					ON (IOF.TABLE_NAME = I.Table_Name 
						AND IOF.OWNER = i.Owner 
						AND IOF.INDEX_NAME <> I.INDEX_NAME)
				WHERE I.OWNER = v_Segment.Owner
				AND I.INDEX_NAME = v_Segment.SEGMENT_NAME
				GROUP BY I.INDEX_TYPE, I.TABLE_NAME, IOF.INDEX_NAME
			);

			IF v_IndexType = 'IOT - TOP' THEN
			
				v_runpoint := 'Moving IOT';
				v_SQL := 'ALTER TABLE ' || v_Segment.Owner || '.' || v_TableName || ' MOVE TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || CASE WHEN v_IOTOverFlow IS NOT NULL THEN ' OVERFLOW TABLESPACE ' || NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) END || ' NOLOGGING';
			ELSE 
				v_runpoint := 'Moving Primary Key of non-partitioned';
				v_SQL := 'ALTER INDEX ' || v_Segment.Owner || '.' || v_Segment.SEGMENT_NAME || ' REBUILD TABLESPACE '|| NVL(p_DestTablespace, v_Segment.TABLESPACE_NAME) || ' NOLOGGING' || v_ParallelClause;
			END IF;
			RUN_STATEMENT(v_LogFileHandle, v_runpoint, v_SQL);

		END IF;
		p_moved := 1;
	EXCEPTION
		WHEN OTHERS THEN
			p_moved := 0;
		dbms_output.PUT_LINE('v_runpoint:' || v_runpoint);
		dbms_output.PUT_LINE('SQLERRM:' || SQLERRM);
		dbms_output.PUT_LINE(v_SQL);
	END move_segment;

BEGIN

	SELECT ts#
	INTO v_TS#
	FROM ts$
	WHERE name = v_SourceTablespace;
	
	IF v_SourceFileID IS NULL THEN
		SELECT CASE WHEN v_SourceTablespace = 'ACTIX_SHRINK_TABLESPACE' THEN MAX(file#) ELSE LEAST(MAX(relfile#),MAX(file#)) END
		INTO v_SourceFileID
		FROM file$
		WHERE ts# = v_TS#;
	END IF;
	
	EXECUTE IMMEDIATE 'alter session set "_optimizer_cartesian_enabled" = false';
	EXECUTE IMMEDIATE 'alter session set "_smm_auto_cost_enabled" = false';

	OPEN_LOG_FILE;
	
	v_runpoint := 'Setting DBMS_METADATA TRANSFORM_PARAMs';
	DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'STORAGE',false);
	DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'SEGMENT_ATTRIBUTES',false);
	DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'TABLESPACE',false);
	DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'REF_CONSTRAINTS',false);
	DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'PARTITIONING',false);

	LOG(v_LogFileHandle,v_runpoint);

	-- CREATE_SEGMENT_LIST;
	
	v_runpoint := 'Calculating BLOCKS threshold for Parallelism';
	EXECUTE IMMEDIATE 'SELECT 5 * ROUND(AVG(BLOCKS)) FROM ACTIX_SHRINK_EXTENTS'
	INTO v_ParallelThreshold;

	CHANGE_DEFAULT_TABLESPACE('TABLE');
	CHANGE_DEFAULT_TABLESPACE('INDEX');
	
	EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DDL';
	EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

	v_SQL := 'SELECT COUNT(*)
				FROM 
				(
					SELECT TABLESPACE_NAME, OWNER, SEGMENT_TYPE, SEGMENT_NAME, PARTITION_NAME, MAX(BLOCK_ID) BLOCKID, SUM(BLOCKS) BLOCKS, IOT, IOT_SEGMENT_NAME
					FROM ACTIX_SHRINK_EXTENTS 
					GROUP BY TABLESPACE_NAME, OWNER, SEGMENT_TYPE, SEGMENT_NAME, PARTITION_NAME, IOT, IOT_SEGMENT_NAME
				)';
	EXECUTE IMMEDIATE v_SQL INTO v_TotalSegmentCount;
	
	GRANT_TABLESPACE_QUOTA;
	
	v_runpoint := 'Moving segments';
	OPEN C_Cursor FOR 'SELECT TABLESPACE_NAME, OWNER, SEGMENT_TYPE, SEGMENT_NAME, PARTITION_NAME, MAX(BLOCK_ID) BLOCKID, SUM(BLOCKS) BLOCKS, IOT, IOT_SEGMENT_NAME
						FROM ACTIX_SHRINK_EXTENTS 
						GROUP BY TABLESPACE_NAME, OWNER, SEGMENT_TYPE, SEGMENT_NAME, PARTITION_NAME, IOT, IOT_SEGMENT_NAME
						ORDER BY 6 DESC';
	LOOP
		FETCH C_Cursor INTO v_Segment;
		v_SegmentCount := v_SegmentCount + 1;
		EXIT WHEN C_Cursor%NOTFOUND;
		
		LOG(v_LogFileHandle,TO_CHAR(v_SegmentCount) || ' of ' || TO_CHAR(v_TotalSegmentCount) || ': Moving ' || v_Segment.OWNER || '.' || v_Segment.SEGMENT_NAME || CASE WHEN v_Segment.PARTITION_NAME IS NOT NULL THEN ' PARTITION(' || v_Segment.PARTITION_NAME || ')' END || ' - HWM(GB):' || TO_CHAR(ROUND(v_Segment.BLOCKID * 32 /(1024 * 1024))) );
		move_segment(v_Segment,v_DestinationTablespace,v_moved);
		if v_moved = 0 then
			v_msg := v_Segment.owner || '.' || v_Segment.segment_name || '(' || v_Segment.partition_name || ')';
			LOG(v_LogFileHandle,'Exception happenned when moving ' || v_msg);
			LOG(v_LogFileHandle,'SQLERRM:' || SQLERRM);
			LOG(v_LogFileHandle,'v_runpoint:' || v_runpoint);
			dbms_output.put_line(v_msg);
			dbms_output.put_line(v_runpoint);
			exit;
		end if;

	END LOOP;
	CLOSE C_Cursor;

	IF v_moved <> 0  THEN
		MOVE_REMAINING_SEGMENTS('TABLE');
		MOVE_REMAINING_SEGMENTS('INDEX');
	END IF;
	
	EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DDL';
	EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

	CLOSE_LOG_FILE;

	EXCEPTION
	WHEN OTHERS THEN
		IF C_Cursor%ISOPEN THEN
			CLOSE C_Cursor;
		END IF;
		dbms_output.PUT_LINE('v_runpoint:' || v_runpoint);
		dbms_output.PUT_LINE('SQLERRM:' || SQLERRM);
		dbms_output.PUT_LINE(v_SQL);
		EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DDL';
		EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';
		CLOSE_LOG_FILE;
		
END;
/

exit