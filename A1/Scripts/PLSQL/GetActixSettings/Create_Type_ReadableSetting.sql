DROP TYPE ReadableSettings;
CREATE OR REPLACE TYPE ReadableSetting AS OBJECT
(
	SchemaName		varchar2(30),
	SettingKey		varchar2(1020),
	UserID			varchar2(1020),
	Value			varchar2(4000)
)
/
SHOW ERRORS

