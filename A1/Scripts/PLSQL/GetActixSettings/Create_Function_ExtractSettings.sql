CREATE OR REPLACE FUNCTION ExtractSettings
(
  p_schema  varchar2, 
  userID    varchar2, 
  settingKey  varchar2
)
RETURN VARCHAR2
AS LANGUAGE JAVA NAME 'Setting.getStringValue(java.lang.String, java.lang.String, java.lang.String) return java.lang.String';
/

SHOW ERRORS


