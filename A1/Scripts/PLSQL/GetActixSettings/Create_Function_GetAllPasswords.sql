CREATE OR REPLACE FUNCTION GetAllPasswords
RETURN SchemaReadableSettings PIPELINED
IS
  v_SchemaReadableSettings  SchemaReadableSettings := SchemaReadableSettings();
  v_SchemaReadableSetting	SchemaReadableSetting:= SchemaReadableSetting(null, null, null, null, null);
  v_cnt INTEGER;  
BEGIN
		
	FOR l_setting IN (
						SELECT SchemaName, UserID, 
							DECODE(REPLACE(Username,' ','*'),null,'server.orchestrator.serviceUser does not exist!',Username) Username, 
							NVL(Password,'server.orchestrator.servicePassword does not exist!') Password,
							CASE UPPER(Username) 
								WHEN UPPER('ActixOneService1')
									THEN 
										CASE WHEN NVL(Password,' ') <> 'MakeTheServ1ce' THEN 'YES' ELSE '-' END
								WHEN UPPER('ActixOneService2')
									THEN 
										CASE WHEN NVL(Password,' ') <> 'l0nd0nW6' THEN 'YES' ELSE '-' END
								WHEN UPPER('ActixOneService3')
									THEN 
										CASE WHEN NVL(Password,' ') <> 'KualaLumpur14' THEN 'YES' ELSE '-' END
								WHEN UPPER('ActixOneService4')
									THEN 
										CASE WHEN NVL(Password,' ') <> '50uth9ark.' THEN 'YES' ELSE '-' END
								WHEN UPPER('ActixOnePM1')
									THEN 
										CASE WHEN NVL(Password,' ') <> 'l0nd0nW6pm' THEN 'YES' ELSE '-' END
								WHEN UPPER('${RemoteServiceUser}')
									THEN 'YES'
								ELSE
									'YES'
							END Needs_To_Be_Checked
						FROM
						(
							SELECT SchemaName, UserID, SettingKey,Username, Password
							FROM 
							(	
								SELECT SchemaName, UserID, 
										SettingKey, lead(SettingKey) over(partition by SchemaName, UserID order by settingKey desc),
										value Username, LEAD(value) over(PARTITION BY SchemaName, UserID ORDER BY SettingKey DESC) Password
								FROM
								(
									SELECT *
									FROM TABLE(GetReadableSettingValue('server.orchestrator.serviceUser'))
									UNION
									SELECT *
									FROM TABLE(GetReadableSettingValue('server.orchestrator.servicePassword'))
								)
								ORDER BY 1, 2
							)
							WHERE SettingKey = 'server.orchestrator.serviceUser'
						)
					) LOOP
		v_SchemaReadableSetting.SchemaName := l_setting.SchemaName;
		v_SchemaReadableSetting.UserID := l_setting.UserID;
		v_SchemaReadableSetting.Username := l_setting.Username;
		v_SchemaReadableSetting.Password := l_setting.Password;
		v_SchemaReadableSetting.Needs_To_Be_Checked := l_setting.Needs_To_Be_Checked;
		v_SchemaReadableSettings.EXTEND;
		v_SchemaReadableSettings(v_SchemaReadableSettings.COUNT) := v_SchemaReadableSetting;
	END LOOP;
  
	FOR l_record IN 1..v_SchemaReadableSettings.COUNT LOOP
		PIPE ROW(v_SchemaReadableSettings(l_record));
	END LOOP;
END GetAllPasswords;
/

SHOW ERRORS


