conn / as sysdba

SET SERVEROUTPUT ON

@@Create_Type_ReadableSetting.sql
@@Create_Type_ReadableSettings.sql
@@Create_Type_SchemaReadableSetting.sql
@@Create_Type_SchemaReadableSettings.sql
@@Create_Java_Source_Point.sql
@@Create_Java_Source_Setting.sql
@@Create_Function_ExtractSettings.sql
@@Create_Procedure_SetServicePassword.sql
@@Create_Procedure_ChangeAllServicePasswords.sql
@@Create_Function_GetReadableSettingValue.sql
@@Create_Function_GetAllPasswords.sql

exit

