CREATE OR REPLACE AND RESOLVE JAVA SOURCE NAMED Setting AS

import java.sql.*;
import oracle.jdbc.*;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Date;
import java.util.LinkedHashMap;
import java.lang.String;

public class Setting
{
  public static String getStringValue(String schema, String userID, String settingKey) throws Exception
  {
    String exceptionFlag = null;
    Connection conn = null;
    InputStream is = null;
    ObjectInputStream oip = null;
    Object object = null;
    String className = null;
    HashMap<Object, Object> val = null;
    String READ_OBJECT_SQL = "SELECT VALUE FROM " + schema + ".SETTINGS WHERE USERID = '" + userID + "' AND UPPER(KEY) LIKE '%" + settingKey.toUpperCase() + "%'";
    String Output = null;
    conn = DriverManager.getConnection("jdbc:default:connection:");
    conn.setAutoCommit(false);
    PreparedStatement pstmt = conn.prepareStatement(READ_OBJECT_SQL);
    ResultSet rs = pstmt.executeQuery();
    rs.next();
    try {
      is = rs.getBlob(1).getBinaryStream();
      oip = new ObjectInputStream(is);
      object = oip.readObject();
      exceptionFlag = "04";
      className = object.getClass().getName();
      exceptionFlag = "05";
      if (className.equals("java.lang.String") || className.equals("java.lang.Integer") || className.equals("java.lang.Boolean")|| className.equals("java.lang.Double") || className.equals("java.util.Date") || className.equals("com.actix.general.Point") || className.equals("java.util.HashMap") || className.equals("java.util.LinkedHashMap")){
        if (object.toString().length() > 4000) {
          exceptionFlag = "06";
          Output = object.toString().substring(0,3995) + "...";
        } else {
          exceptionFlag = "07";
          Output = object.toString();
        }
      } else {
        Output = "Unsupported object type! - " + className;
      }
      exceptionFlag = "08";
      oip.close();
      exceptionFlag = "09";
      is.close();
    } catch (NullPointerException e){
      Output = "null";
    } catch (ClassNotFoundException e){
      Output = "Unsupported object type! - " + e.toString().substring(e.toString().indexOf(":") + 2);
    } catch (Exception exception) {
      Output = "Exception(flag " + exceptionFlag + "): " + exception;
    }
    rs.close();
    pstmt.close();
      
    conn.close();
    return Output;
  }
  
  public static void setServicePassword(String schema, String userID, String password) throws Exception
  {
    String UPDATE_OBJECT_SQL = "UPDATE " + schema + ".SETTINGS SET VALUE = ? WHERE USERID = '" + userID + "' AND KEY = 'server.orchestrator.servicePassword'";
	PreparedStatement pstmt = null;
	Connection conn = null;
    try {
		conn = DriverManager.getConnection("jdbc:default:connection:");
		conn.setAutoCommit(false);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(password);
		byte[] passwordAsBytes = baos.toByteArray();
		
		pstmt = conn.prepareStatement(UPDATE_OBJECT_SQL);
		
		ByteArrayInputStream is = new ByteArrayInputStream(passwordAsBytes);
		
		pstmt.setBinaryStream(1,is, passwordAsBytes.length);
		pstmt.executeUpdate();
		
    } catch (Exception e) {
		assert true;
    }
    pstmt.close();
    conn.close();
  }
}
/


SHOW ERRORS;


