CREATE OR REPLACE PROCEDURE SetServicePassword
(
  p_schema  varchar2, 
  userID    varchar2, 
  password  varchar2
)
AS LANGUAGE JAVA NAME 'Setting.setServicePassword(java.lang.String, java.lang.String, java.lang.String)';
/

SHOW ERRORS


