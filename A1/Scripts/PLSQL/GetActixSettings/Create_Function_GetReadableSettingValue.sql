CREATE OR REPLACE FUNCTION GetReadableSettingValue
(
  p_Key         IN  varchar2 DEFAULT NULL,
  p_UserID      IN  varchar2 DEFAULT NULL,
  p_SchemaName  IN  varchar2 DEFAULT NULL
)
RETURN ReadableSettings PIPELINED
IS
  v_ReadableSettings  ReadableSettings;
  TYPE t_UserIDs  IS TABLE OF VARCHAR2(4000);
  v_UserIDs t_UserIDs;
  TYPE t_UserNames	IS TABLE OF VARCHAR2(30);
  v_UserNames t_UserNames;
BEGIN

	SELECT USERNAME
	BULK COLLECT INTO v_UserNames
	FROM DBA_USERS U
	WHERE USERNAME LIKE 'ACTIX%'
	AND ACCOUNT_STATUS <> 'LOCKED' 
	AND USERNAME = CASE WHEN p_SchemaName IS NULL THEN USERNAME ELSE p_SchemaName END
	AND EXISTS (SELECT NULL
				FROM DBA_OBJECTS
				WHERE OWNER = U.USERNAME
				AND OBJECT_TYPE = 'TABLE'
				AND OBJECT_NAME = 'SETTINGS')
	ORDER BY 1;

	FOR l_user IN 1..v_UserNames.COUNT LOOP
		EXECUTE IMMEDIATE 'SELECT ReadableSetting(''' || v_UserNames(l_user) || ''',Key, UserID, NULL) FROM ' || v_UserNames(l_user) || '.Settings WHERE UPPER(Key) LIKE ''%'' || UPPER(:p_Key) || ''%'' AND UserID = NVL(:p_UserID, UserID) ORDER BY UserID' 
		BULK COLLECT INTO v_ReadableSettings
		USING p_Key, p_UserID;

		FOR l_recordIdx IN 1..v_ReadableSettings.COUNT LOOP
			v_ReadableSettings(l_recordIdx).Value := SUBSTR(ExtractSettings(v_UserNames(l_user), v_ReadableSettings(l_recordIdx).UserID, v_ReadableSettings(l_recordIdx).SettingKey),1,3995);
		END LOOP;

		FOR l_record IN 1..v_ReadableSettings.COUNT LOOP
			PIPE ROW(v_ReadableSettings(l_record));
		END LOOP;
	END LOOP;
END GetReadableSettingValue;
/

SHOW ERRORS


