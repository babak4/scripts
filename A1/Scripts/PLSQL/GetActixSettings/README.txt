This utility should be installed as SYS user:

sqlplus /nolog @install.sql

Sample Queries - should be run as SYS user (sqlplus / as sysdba)

SELECT *
FROM TABLE(GetReadableSettingValue());

SELECT *
FROM TABLE(GetReadableSettingValue('pass'));

SELECT *
FROM TABLE(GetReadableSettingValue(null, 'ActixOne'));

SELECT *
FROM TABLE(GetReadableSettingValue('orchestrator', 'ActixOne'));

SELECT *
FROM TABLE(GetAllPasswords)
WHERE NEEDS_TO_BE_CHECKED = 'YES';

