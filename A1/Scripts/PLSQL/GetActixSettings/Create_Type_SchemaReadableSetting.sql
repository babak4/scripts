DROP TYPE SchemaReadableSettings;

CREATE OR REPLACE TYPE SchemaReadableSetting AS OBJECT
(
	SchemaName			varchar2(30),
	UserID				varchar2(1020),
	Username			varchar2(1020),
	Password			varchar2(1020),
	Needs_To_Be_Checked	varchar2(3)
)
/

SHOW ERRORS

