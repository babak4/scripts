CREATE OR REPLACE AND RESOLVE JAVA SOURCE NAMED Point AS

package com.actix.general;

import java.io.Serializable;

public class Point implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static final int NUM_FIELDS = 2;
	private static final String ARRAY_DESCRIPTOR = "POINTARRAY";
	
    // mutability and no-args constructor is required by Jax-WS

	public Double x;
	public Double y;

	public Point()
	{
	}

	public Point(Double x, Double y)
	{
		this.x = x;
		this.y = y;
	}

    public String toString()
    {
        return "(" + x + "," + y + ")";
    }

	public Object[] asRecord()
	{
		return new Object[]{x, y};
	}
	
}
/

SHOW ERRORS;

