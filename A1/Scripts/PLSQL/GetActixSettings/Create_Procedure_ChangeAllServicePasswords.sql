CREATE OR REPLACE PROCEDURE ChangeAllServicePasswords
IS
BEGIN
	FOR l_setting IN (
		SELECT SchemaName, UserID, 
			DECODE(REPLACE(Username,' ','*'),null,'server.orchestrator.serviceUser does not exist!',Username) Username, 
			NVL(Password,'server.orchestrator.servicePassword does not exist!') Password
		FROM
		(
			SELECT SchemaName, UserID, SettingKey,Username, Password
			FROM 
			(	
				SELECT SchemaName, UserID, 
						SettingKey, lead(SettingKey) over(partition by SchemaName, UserID order by settingKey desc),
						value Username, LEAD(value) over(PARTITION BY SchemaName, UserID ORDER BY SettingKey DESC) Password
				FROM
				(
					SELECT *
					FROM TABLE(GetReadableSettingValue('server.orchestrator.serviceUser'))
					UNION
					SELECT *
					FROM TABLE(GetReadableSettingValue('server.orchestrator.servicePassword'))
				)
				ORDER BY 1, 2
			)
			WHERE SettingKey = 'server.orchestrator.serviceUser'
		)
	) LOOP
	
		IF UPPER(l_setting.Username) = UPPER('ActixOneService1') AND NVL(l_setting.Password,' ') <> 'MakeTheServ1ce' THEN 
			SetServicePassword(l_setting.SchemaName, l_setting.UserID, 'MakeTheServ1ce');
		ELSIF UPPER(l_setting.Username) = UPPER('ActixOneService2') AND NVL(l_setting.Password,' ') <> 'l0nd0nW6' THEN
			SetServicePassword(l_setting.SchemaName, l_setting.UserID, 'l0nd0nW6');
		ELSIF UPPER(l_setting.Username) = UPPER('ActixOneService3') AND NVL(l_setting.Password,' ') <> 'KualaLumpur14' THEN
			SetServicePassword(l_setting.SchemaName, l_setting.UserID, 'KualaLumpur14');
		ELSIF UPPER(l_setting.Username) = UPPER('ActixOneService4') AND NVL(l_setting.Password,' ') <> '50uth9ark.' THEN
			SetServicePassword(l_setting.SchemaName, l_setting.UserID, '50uth9ark');
		ELSIF UPPER(l_setting.Username) = UPPER('ActixOnePM1') AND NVL(l_setting.Password,' ') <> 'l0nd0nW6pm' THEN
			SetServicePassword(l_setting.SchemaName, l_setting.UserID, 'l0nd0nW6pm');
		END IF;
	
	END LOOP;
	
	COMMIT;
	
END ChangeAllServicePasswords;
/

SHOW ERRORS


