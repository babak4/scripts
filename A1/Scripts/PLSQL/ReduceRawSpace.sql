SET SERVEROUTPUT ON
DECLARE
	v_count integer;
	v_total_blocks  integer;
	v_total_bytes integer;
	v_unused_blocks integer;
	v_unused_bytes integer;
	v_temp1 integer;
	v_temp2 integer;
	v_temp3 integer;
	v_DTQuery1	NUMBER;
	v_DTQuery2	NUMBER;
	v_TableNames  NAMEARRAY;
BEGIN
	BEGIN
		EXECUTE IMMEDIATE 'SELECT TABLENAME FROM DTQUERY WHERE COALESCE(tableinitialextent, tablenextextent, indexinitialextent, indexnextextent) IS NOT NULL'
		BULK COLLECT INTO v_TableNames;
	EXCEPTION
		WHEN OTHERS
			THEN
				EXECUTE IMMEDIATE 'SELECT TABLENAME FROM DTQUERY WHERE COALESCE(tableinitialextent, tablenextextent) IS NOT NULL'
				BULK COLLECT INTO v_TableNames;
	END;

	SELECT CASE WHEN LISTAGG(COLUMN_NAME,' ') WITHIN GROUP (ORDER BY COLUMN_ID) LIKE '%TABLEINITIALEXTENT%' THEN 1 ELSE 0 END,CASE WHEN LISTAGG(COLUMN_NAME,' ') WITHIN GROUP (ORDER BY COLUMN_ID) LIKE '%INDEXINITIALEXTENT%' THEN 1 ELSE 0 END
	INTO v_DTQuery1, v_DTQuery2
	FROM USER_TAB_COLS
	WHERE TABLE_NAME='DTQUERY';

	IF v_DTQuery2 <> 0 OR v_DTQuery1 <> 0 THEN 
		EXECUTE IMMEDIATE 'ALTER TRIGGER TR_DTQUERY_TABLEDEF DISABLE';

		IF v_DTQuery2 <> 0 THEN
			EXECUTE IMMEDIATE 'UPDATE DTQuery SET TableInitialExtent = NULL, TableNextExtent = NULL, IndexInitialExtent = NULL, IndexNextExtent = NULL
			WHERE COALESCE(tableinitialextent, tablenextextent, indexinitialextent, indexnextextent) IS NOT NULL';
		ELSE
			EXECUTE IMMEDIATE 'UPDATE DTQuery SET TableInitialExtent = NULL, TableNextExtent = NULL
			WHERE COALESCE(tableinitialextent, tablenextextent) IS NOT NULL';
		END IF;

		EXECUTE IMMEDIATE 'ALTER TRIGGER TR_DTQUERY_TABLEDEF ENABLE';
	END IF;

	UPDATE DBAdmin_Parameter SET Float_Value = 2 WHERE Parameter_Name = 'AGG_CORE_MIN_TABLES' AND Float_Value > 2;
	UPDATE DBAdmin_Parameter SET Float_Value = 4 WHERE Parameter_Name = 'AGG_CORE_MAX_TABLES' AND Float_Value > 4;
	UPDATE DBAdmin_Parameter SET Float_Value = 2 WHERE Parameter_Name = 'AGG_CORE_MIN_FREE_TABLES' AND Float_Value > 2;
	UPDATE DBAdmin_Parameter SET Float_Value = 1 WHERE Parameter_Name = 'AGG_CORE_MIN_AVAILABLE_TABLES' AND Float_Value > 1;
	UPDATE DBAdmin_Parameter SET Float_Value = 25 WHERE Parameter_Name = 'AGG_STREAM_PART_RESIZE' AND Float_Value > 25;
	UPDATE DBAdmin_Parameter SET Float_Value = 100 WHERE Parameter_Name = 'AGG_STREAM_PART_MIN_SIZE' AND Float_Value > 100;
	UPDATE DBAdmin_Parameter SET Float_Value = 25 WHERE Parameter_Name = 'AGG_STREAM_PART_MIN_FREE' AND Float_Value > 25;
	UPDATE DBAdmin_Parameter SET Float_Value = 1000 WHERE Parameter_Name = 'AGG_STREAM_PART_MAX_SIZE' AND Float_Value > 1000;

	COMMIT;
	FOR l_table_name IN (SELECT COLUMN_VALUE FROM TABLE(v_TableNames)) LOOP
		dbms_output.put_line(l_table_name.COLUMN_VALUE);
		FOR l_drop_raw IN (SELECT 'DROP TABLE ' || TABLE_NAME || ' PURGE' statement
							FROM USER_TAB_PARTITIONS
							WHERE TABLE_NAME LIKE UPPER(l_table_name.COLUMN_VALUE) || '%'
							GROUP BY TABLE_NAME) LOOP
			EXECUTE IMMEDIATE l_drop_raw.statement;
		END LOOP;
	END LOOP;

	DELETE FROM DTStreamPartition;
	DELETE FROM DTStagingTableConfig;
	COMMIT;
	Table_Manager.ManageRawPartitions;

EXCEPTION 
	WHEN OTHERS 
		THEN
			DBMS_OUTPUT.PUT_LINE(SQLCODE||' -ERROR- '||SQLERRM);
			EXECUTE IMMEDIATE 'ALTER TRIGGER TR_DTQUERY_TABLEDEF ENABLE';
END;
/


